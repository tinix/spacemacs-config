
# Atajos de teclado	Descripción
```
[ SPC	Insertar espacio arriba
] SPC	Insertar espacio debajo
[ b	Ir al búfer anterior
] b	Ir al siguiente búfer
[ f	Ir al archivo anterior en el directorio
] f	Ir al siguiente archivo en el directorio
[ l	Ir al error anterior
] l	Ir al siguiente error
[ h	Ir al trozo vcs anterior
] h	Ir al siguiente trozo de vcs
[ q	Ir al error anterior
] q	Ir al siguiente error
[ t	Ir al cuadro anterior
] t	Ir al siguiente cuadro
[ w	Ir a la ventana anterior
] w	Ir a la ventana siguiente
[ e	Mover línea
] e	Mover línea hacia abajo
[ p	Pegar sobre la línea actual
] p	Pegar debajo de la línea actual
g p	Seleccionar texto pegado
```

### 14.5.4 Salto, unión y división
 * El SPC jprefijo es para saltar, unir y dividir.

* 14.5.4.1 Salto
### Clave de enlace	Descripción
``` 
SPC j 0	ir al comienzo de la línea (y establecer una marca en la ubicación anterior de la línea)
SPC j $	ir al final de la línea (y establecer una marca en la ubicación anterior de la línea)
SPC j b	deshacer un salto (volver a la ubicación anterior)
SPC j d	saltar a una lista del directorio actual
SPC j D	saltar a una lista del directorio actual (otra ventana)
SPC j f	saltar a la definición de una función Emacs Lisp
SPC j i	saltar a una definición en el búfer (imenu)
SPC j I	saltar a una definición en cualquier búfer (imenu)
SPC j j	saltar a un personaje en el búfer (funciona como un movimiento maligno)
SPC j J	saltar a un conjunto de dos personajes en el búfer (funciona como un movimiento maligno)
SPC j k	salta a la siguiente línea y sangra con reglas de sangría automática
SPC j l	saltar a una línea con avy (funciona como un movimiento maligno)
SPC j q	muestra la información sobre herramientas de vista rápida de dumb-jump
SPC j u	saltar a una URL en el búfer actual
SPC j v	saltar a la definición / declaración de una variable Emacs Lisp
SPC j w	saltar a una palabra en el búfer actual (funciona como un movimiento maligno)
```


### 14.5.4.2 Unir y dividir
### Clave de enlace	Descripción
```
J	une la línea actual con la siguiente línea
SPC j k	vaya a la línea siguiente y aplique sangría usando reglas de sangría automática
SPC j n	dividir la línea actual en el punto, insertar una nueva línea y sangrar automáticamente
SPC j s	dividir una cadena entre comillas o expresión-s en su lugar
SPC j S	dividir una cadena entre comillas o expresión-s, insertar una nueva línea y sangrar automáticamente
```

14.5.5 Manipulación de ventanas
14.5.5.1 Vinculaciones de teclas de manipulación de ventanas
Cada ventana tiene un número que se muestra al comienzo de la línea de modo y se puede acceder rápidamente usando SPC number.

### Clave de enlace	Descripción
```
SPC 1	ir a la ventana número 1
SPC 2	ir a la ventana número 2
SPC 3	ir a la ventana número 3
SPC 4	ir a la ventana número 4
SPC 5	ir a la ventana número 5
SPC 6	ir a la ventana número 6
SPC 7	ir a la ventana número 7
SPC 8	ir a la ventana número 8
SPC 9	ir a la ventana número 9
SPC 0	ir a la ventana número 0
```

### Comandos de manipulación de Windows (comience con w):

### Clave de enlace	Descripción
```
SPC w =	equilibrar ventanas divididas
SPC w b	forzar el enfoque de nuevo al minibúfer (útil con helmventanas emergentes)
SPC w c	maximizar / minimizar una ventana y centrarla
SPC w C	maximizar / minimizar una ventana y centrarla usando ace-window
SPC w d	eliminar una ventana
SPC u SPC w d	eliminar una ventana y su búfer actual (no elimina el archivo)
SPC w D	eliminar otra ventana usando ace-window
SPC u SPC w D	eliminar otra ventana y su búfer actual usando ace-window
SPC w t	alternar la dedicación de la ventana (una ventana dedicada no puede ser reutilizada por un modo)
SPC w f	alternar el modo de seguimiento
SPC w F	crear nuevo marco
SPC w h	mover a la ventana de la izquierda
SPC w H	mover ventana a la izquierda
SPC w j	mover a la ventana de abajo
SPC w J	mover la ventana hacia abajo
SPC w k	mover a la ventana de arriba
SPC w K	mover la ventana hacia arriba
SPC w l	mover a la ventana a la derecha
SPC w L	mover ventana a la derecha
SPC w m	maximizar / minimizar una ventana (maximizar es equivalente a eliminar otras ventanas)
SPC w M	intercambiar ventanas usando ace-window
SPC w o	ciclo y enfoque entre cuadros
SPC w p m	abrir el búfer de mensajes en una ventana emergente
SPC w p p	cerrar la ventana emergente adhesiva actual
SPC w r	rotar ventanas hacia adelante
SPC w R	gire las ventanas hacia atrás
SPC w s o SPC w -	división horizontal
SPC w S	división horizontal y foco nueva ventana
SPC w u	deshacer el diseño de la ventana (utilizado para deshacer efectivamente una ventana cerrada)
SPC w U	rehacer el diseño de la ventana
SPC w v o SPC w /	división vertical
SPC w V	división vertical y foco nueva ventana
SPC w w	ciclo y enfoque entre ventanas
SPC w W	seleccionar ventana usando ace-window
```

### 14.5.5.2 Estado transitorio de manipulación de ventanas
* Un conveniente estado transitorio de manipulación de ventana permite realizar la mayoría de las acciones enumeradas anteriormente. El estado transitorio permite acciones adicionales, así como el cambio de tamaño de la ventana.

### Clave de enlace	Descripción
```
SPC w .	iniciar estado transitorio
?	mostrar la documentación completa en minibúfer
0	ir a la ventana número 0
1	ir a la ventana número 1
2	ir a la ventana número 2
3	ir a la ventana número 3
4	ir a la ventana número 4
5	ir a la ventana número 5
6	ir a la ventana número 6
7	ir a la ventana número 7
8	ir a la ventana número 8
9	ir a la ventana número 9
/	división vertical
-	división horizontal
[	reducir la ventana horizontalmente
]	ampliar ventana horizontalmente
{	reducir la ventana verticalmente
}	ampliar ventana verticalmente
d	eliminar ventana
D	eliminar otras ventanas
g	activar golden-ratioy desactivar
h	ve a la ventana de la izquierda
j	ir a la ventana de abajo
k	ir a la ventana de arriba
l	ve a la ventana de la derecha
H	mover ventana a la izquierda
J	mover la ventana hacia abajo
K	mover de abajo hacia arriba
L	mover ventana a la derecha
o	enfocar otro marco
r	rotar ventanas hacia adelante
R	gire las ventanas hacia atrás
s	división horizontal
S	división horizontal y foco nueva ventana
u	deshacer el diseño de la ventana (utilizado para deshacer efectivamente una ventana cerrada)
U	rehacer el diseño de la ventana
v	división vertical
V	división horizontal y foco nueva ventana
w	enfocar otra ventana
```

* Cualquier otra llave	dejar el estado transitorio

### 14.5.6 Buffers y archivos
* Por defecto, Spacemacs utiliza helmpara abrir archivos.

### 14.5.6.1 Búferes de manipulación de teclas vinculadas
* Comandos de manipulación del búfer (comenzar con b):

* Clave de enlace	Descripción
```
SPC TAB	cambiar a búfer alternativo en la ventana actual (cambiar de un lado a otro)
SPC b b	cambiar a un búfer con timón
SPC b d	matar el búfer actual (no elimina el archivo visitado)
SPC u SPC b d	matar el búfer y la ventana actuales (no elimina el archivo visitado)
SPC b D	matar un búfer visible usando ace-window
SPC u SPC b D	matar un búfer visible y su ventana usando ace-window
SPC b C-d	matar buffers usando una expresión regular
SPC b e	borrar el contenido del búfer (pedir confirmación)
SPC b h	abrir *spacemacs*el buffer de inicio
SPC b n	cambiar al siguiente búfer evitando los búferes especiales
SPC b m	matar todos los buffers excepto el actual
SPC u SPC b m	matar todos los búferes y ventanas excepto el actual
SPC b M	matar a todos los buffers que coincidan con la expresión regular
SPC b p	cambiar al buffer anterior evitando buffers especiales
SPC b P	copiar el portapapeles y reemplazar el búfer (útil al pegar desde un navegador)
SPC b R	revertir el búfer actual (recargar desde el disco)
SPC b s	cambie al *scratch*búfer (créelo si es necesario)
SPC b w	alternar solo lectura (estado de escritura)
SPC b Y	Copiar todo el búfer en el portapapeles (útil al copiar en un navegador)
z f	Haga que la función actual o los comentarios sean visibles en el búfer tanto como sea posible
```

-- 14.5.6.2 Estado transitorio de manipulación de buffers
-- Un conveniente estado transitorio de manipulación del búfer permite pasar rápidamente por el búfer abierto y matarlos.

* Clave de enlace	Descripción
```
SPC b .	iniciar estado transitorio
K	matar el búfer actual
n	vaya al siguiente búfer (evite los búferes especiales)
N	ir al búfer anterior (evitar búferes especiales)
```
Cualquier otra llave	dejar el estado transitorio
